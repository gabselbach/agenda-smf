<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Collection;
use DB;
use Carbon\Carbon;
use App\Agenda;

class ApiController extends Controller
{

  /* FUNÇÃO QUE CARREGAVA O CONTEÚDO DO FAQ DIRETAMENTE DO BANCO
  NESTE MOMENTO ESTÁ FUNÇÃO NÃO ESTÁ SENDO USADA, MAS ESTA FUNCIONANDO PERFEITAMENTE 
  CASO PRECISE UTILIZA-LÁ 
  */
  public function faq(Request $request){
    $id_faq = $request->faq;
     $strOP = "op=DUVIDA;FAQ={$id_faq}";
     $body = $this->RequestWebservice($strOP);
    if ($body->dados->retorno)
      return ['texto' => $body->dados->retorno];
    else
      return ['texto' => ""];
  }
  public function trataSetor($setor){
    switch ($setor){
      case 'GERAL':
        $setor = 1;
        break;
      case 'IPTU':
        $setor = 2;
        break;
      case 'ISSQN':
        $setor = 3;
        break;
      case 'ITBI':
        $setor = 4;
        break;
      case 'TAXA':
        $setor = 5;
        break;
        case 'ICMS':
          $setor = 6;
          break;
    }
    return $setor;
  }
  /* FUNÇÃO PARA FAZER A REQUISIÇÃO AO ASTEN PROCESSOS
  VOCÊ SÓ IRÁ PRECISAR MEXER NESSA FUNÇÃO CASO A VERSÃO DO PROCESSO MUDE, O LINK DO ASTEN MUDE
  */
  public function RequestWebservice($strOP)
  {
    
    $client = new \GuzzleHttp\Client();
    $res = $client->request('POST', 'http://processos2.pelotas.rs.gov.br/processos/api/service.action', [
      'headers' => [
        'Content-Type' => 'application/x-www-form-urlencoded',
      ],
      'form_params' => [
        'versao' => '1480',
        'luaToJson[]' => 'dados',
        'varsRetorno[]' => 'dados',
        'deleteAfter' => 'false', // manter instancia 
        'dadosEntrada' => $strOP,
      ]
    ]);
    
    
    $body = json_decode($res->getBody()->getContents());
    return $body;
  }
  // FUNÇÃO PARA LOCALIZAR O CONTRIBUINTE NA BASE DE DADOS, CASO ELE TENHA DÉBITOS COM O MUNICIPIO É INFORADO
  public function procuraPessoa(Request $request)
  {
    $cpf = $request->cpf;
    $cpf = $this->Mask($cpf);
    $strOP = "op=BUSCA_CONTRIB;CNPJ={$cpf}";
    $body = $this->RequestWebservice($strOP);
    if ($body->dados->debitos)
      return ['debitos' => $body->dados->debitos];
    else
      return ['debitos' => "erro inexperado"];
  }
  /* FUNÇÃO PARA REQUISITAR OS HORÁRIOS DISPONÍVEIS
  
  AQUI TEMOS UMA PECULIARIDADE, CONFORME É EXIBIDO UM HORÁRIO PARA O CONTRIBUINTE ELE PODE INFORMAR SE
  QUER OU NÃO AQUELE HÓRARIO, CASO ELE NÃO QUEIRA, ESSE HORÁRIO É INSERIDO NO ARRAY $NAO
  */
  public function getAgendaHorarios(Request $request)
  {
    $setor = $request->setor;
    $turno = $request->turno;
    $nao = $request->nao;
    $setor = $this->trataSetor($setor);
    
    
    if ($turno == 'tarde') {
      $horcom = '12:00';
      $horfim = '18:30';
    } else {
      $horcom = '8:00';
      $horfim = '12:00';
    }
    if(isset($request->cpf)){
      $cpf = $request->cpf;
      $cpf = $this->Mask($cpf);
    }
    
  
    if ($nao) {
      $strOP = "op=BUSCA_HR;HR_INI={$horcom};HR_FIM={$horfim};SETOR={$setor};CNPJ={$cpf};NAO={$nao}";
      $body = $this->RequestWebservice($strOP);
      return [
        'var' => $body->dados
      ];
    } else {
      $strOP = "op=BUSCA_HR;HR_INI={$horcom};HR_FIM={$horfim};SETOR={$setor};CNPJ={$cpf}";
      $body = $this->RequestWebservice($strOP);
      $nao = "";
    }
    if (isset($body->dados->horario)) { 
      if($nao!="")
        $nao = $nao . ',' . $body->dados->horario;
      else
        $nao = $body->dados->horario;
      return [
        'horario' => $body->dados->horario,
       
        'nao' => $nao
      ];
    } else {
      return [
        'horario' => '',
        'nao' => '.'
      ];
    }
  }
  // FUNÇÃO QUE IRÁ SETAR O HORÁRIO ESCOLHIDO PARA AQUELE CONTRIBUINTE
  public function confirmaHorario(Request $request)
  {
    $nome = $request->nome;
    $setor = $request->setor;
    $setor = $this->trataSetor($setor);
    $horario = $request->horario;
    $cpf = $this->Mask($request->cpf);
    $strOP = "op=CONFIRM_HR;HR={$horario};SETOR={$setor};CNPJ={$cpf};NOME={$nome}";
    $body = $this->RequestWebservice($strOP);
    if ($body->dados->response) 
      return ['response' => 'Horário Confirmado : ' . $horario];
    else
      return ['response' => 'Ocorreu um erro inesperado'];
  }
  // FUNÇÃO QUE CARREGA OS HORÁRIOS QUE O CONTRIBUINTE MARCOU
  public function meuHorario(Request $request)
  {
    $setor = $request->setor;
    $cpf = $this->Mask($request->cpf);
    $setor = $this->trataSetor($setor);
    $strOP = "op=MEU_HR;SETOR={$setor};CNPJ={$cpf}";
    $body = $this->RequestWebservice($strOP);
    if (isset($body->dados->horario)) {
      return [
        'horario_marcado' => $body->dados->horario,
        'hr_atual' => $body->dados->horarioAtual,
        'tem' => 'sim'
      ];
    } else {
      return [
        'horario_marcado' => 'Você não possui horário marcado no setor',
        'tem' => 'nao'
      ];
    }
  }
  // FUNÇÃO QUE CANCELA O HORÁRIO DO CONTRIBUINTE
  public function cancelaHorario(Request $request)
  {
    $setor = $this->trataSetor($request->setor);
    $cpf = $this->Mask($request->cpf);
    $horario = $request->horario;
    $strOP = "op=CANCELA_MEU_HR;SETOR={$setor};CNPJ={$cpf};HR={$horario}";
    $body = $this->RequestWebservice($strOP);
    if ($body->dados->response)
      return ['response' => 'Horário Cancelado com sucesso'];
    else
      return ['response' => 'Ocorreu um erro inesperado'];
  }
  
  // MÁSCARA DE CPF/CNPJ
  public function Mask($cpf)
  {
    $mask = '';
    $str = '';
    $pos = strpos($cpf, '.');
    if ($pos === false) {
      if (strlen($cpf) <= 11) {
        while (strlen($cpf) != 11) {
          $cpf = '0' . $cpf;
        }
        $mask = '###.###.###-##';
      } else {
        if (strlen($cpf) < 14) {
          while (strlen($cpf) != 14) {
            $cpf = '0' . $cpf;
          }
          $mask = '##.###.###/####-##';
        }
      }
      $str = str_replace(" ", "", $cpf);

      for ($i = 0; $i < strlen($str); $i++) {
        $mask[strpos($mask, "#")] = $str[$i];
      }
      return $mask;
    } else
      return $cpf;
  }
 
}
