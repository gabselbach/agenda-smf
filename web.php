<?php

Route::post('/search', "ApiController@procuraPessoa"); // PROCURA PESSOA
Route::post('/veHorario', "ApiController@getAgendaHorarios"); //  RECEBE HORÁRIOS
Route::post('/confirma', "ApiController@confirmaHorario"); // CONFIRMA HORÁRIO
Route::POST('/meuhorario', "ApiController@meuHorario"); // MOSTRA HORÁRIO DO CONTRIBUINTE
Route::post('/cancelameuhorario', "ApiController@cancelaHorario"); // CANCELA HORÁRIO 
Route::post('/faq', "ApiController@faq"); // FAQ
